// SPDX-License-Identifier: GPL-2.0+

/*
 * Atomic Modeset standalone testapp
 *
 * Copyright (c) 2022 Madhav Chauhan <madhav.imsec@gmail.com>
 */

#include <errno.h>
#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <time.h>
#include <unistd.h>
#include <ctype.h>
#include <getopt.h>
#include <inttypes.h>
#include <xf86drm.h>
#include <xf86drmMode.h>
#include <drm_fourcc.h>

/**
 * Documentation:
 * -------------
 * This test explain concept of atomic modeset at the low level without using/wrapping functions.
 * In DRM/KMS, Display pipeline consists of following objects:
 *
 * 1. FRAMEBUFFERS:
 * ---------------
 *  This is the place where actual video/pixel data is available. It could be of various formats
 * like RGB, YUV etc.
 *
 * 2. PLANES:
 * ---------
 * It is linked to FRAMEBUFFER with more specific details about visual coordinates. There
 * could be multiple plane associated with a CRTC. Plane could be Primary, Overlay (For video), Cursor.
 *
 * 3. CRTC:
 * -------
 * This takes data from all of its associated planes and composite a final image which can be displayed
 *
 * 4. ENCODER:
 * ----------
 * O/P Image or Pixel stream from CRTC goes to ENCODER so that it can convert to a format which can be
 * understood by CONNECTOR (HDMI/DP/eDP/DSI).
 *
 * 5. CONNECTOR:
 * -------------
 * To which Panels are connected. Connectors converts pixel stream into electric signals.
 *
 * High level flow diagram is shown below:
 *
 * BUF =====> DRM FB =====> DRM Plane ======
 *                                         ||
 *                                         CRTC =====> Encoder =====> Connector =====> Panel
 *                                         ||
 * BUF =====> DRM FB =====> DRM Plane ======
 *
 * How to Compile:
 * ---------------
 *  1. It has dependecy on LIBDRM which can be installed using "sudo apt install libdrm-dev" on Ubuntu
 *  2. $gcc atomic_display_modeset.c -o atomic_display_modeset `pkg-config --libs --cflags libdrm`
 *
 * How to test:
 * ------------
 *  1. We should enter into terminal mode or exit Display Server. Use "Ctrl + Alt + F1 or F2 or F3..
 *  2. Run the test ./atomic_display_modeset.
 *
 * Expected Result:
 * ---------------
 *  If everything goes fine, you should be able to see a rectangle strip on the display panel connected.
*/

static drmModeConnectorPtr get_connector_for_modeset(int drm_fd, drmModeResPtr res_ptr)
{
	int i;
	drmModeConnectorPtr conn = NULL;

	for (i = 0; i < res_ptr->count_connectors; i++) {
		drmModeConnectorPtr conn_ptr = drmModeGetConnector(drm_fd, res_ptr->connectors[i]);
		if (conn_ptr->connection == DRM_MODE_CONNECTED && conn_ptr->count_modes) {
			conn = conn_ptr;
			break;
		}

		drmModeFreeConnector(conn_ptr);
	}

	return conn;
}

static int32_t get_crtc_for_modeset(int drm_fd, drmModeResPtr res_ptr, drmModeConnectorPtr conn_ptr,
				    uint32_t *crtc_id)
{
	int ret;
	drmModeEncoder *enc_ptr;
	uint32_t possible_crtcs;
	int i, j;

	for (i = 0; i < conn_ptr->count_encoders; i++) {
		enc_ptr = drmModeGetEncoder(drm_fd, conn_ptr->encoders[i]);
		possible_crtcs = enc_ptr->possible_crtcs;
		drmModeFreeEncoder(enc_ptr);
		for (j = 0; possible_crtcs >> j; j++)
			if (possible_crtcs & (1 << j)) {
				*crtc_id =  res_ptr->crtcs[j];
				return 0;
			}
	}

	return -1;
}

static int create_display_fb(int32_t drm_fd, uint32_t width, uint32_t height, uint32_t *bo_fbid,
			     void **addr)
{
	struct drm_mode_create_dumb dumb_bo_req = {0};
	struct drm_mode_map_dumb dumb_bo_map_req = {0};
	int ret;
	uint32_t fb_id;
	uint32_t bo_handles[4] = {0};
	uint32_t bo_pitches[4] = {0};
	uint32_t bo_offsets[4] = {0};
	int8_t *bo_va_addr;

	dumb_bo_req.width = width;
	dumb_bo_req.height = height;
	dumb_bo_req.bpp = 32;

	ret = drmIoctl(drm_fd, DRM_IOCTL_MODE_CREATE_DUMB, &dumb_bo_req);
	if(ret) {
		fprintf(stderr, "failed to create dumb buffer: %s\n",
			    strerror(errno));
		return -1;
	}

	bo_handles[0] = dumb_bo_req.handle;
	bo_pitches[0] = dumb_bo_req.pitch;

	/* Add FB using this BO and get the FBID */
	ret = drmModeAddFB2(drm_fd, width, height, DRM_FORMAT_ARGB8888,
			    bo_handles, bo_pitches, bo_offsets, &fb_id, 0);
	if (ret) {
		fprintf(stderr, "failed to add FB: %s\n",
			    strerror(errno));
		return -1;
	}

	dumb_bo_map_req.handle = bo_handles[0];
	ret = drmIoctl(drm_fd, DRM_IOCTL_MODE_MAP_DUMB, &dumb_bo_map_req);
	if (ret) {
		fprintf(stderr, "IOCTL: failed to map BO:%s\n",
			    strerror(errno));
		return -1;
	}

	/* Get the virtual address of the buffer, needed to fill colors */
	bo_va_addr = (int8_t *) mmap(0, dumb_bo_req.size, PROT_READ | PROT_WRITE, MAP_SHARED,
			        drm_fd, dumb_bo_map_req.offset);
	if (bo_va_addr == MAP_FAILED) {
		fprintf(stderr, "MMAP failed to map BO:%s\n",
			    strerror(errno));
		return -1;
	}

	/* clear the framebuffer to 0 */
	memset(bo_va_addr, 0x2000, dumb_bo_req.size);

	*bo_fbid = fb_id;
	*addr = (void *) bo_va_addr;

	return 0;
}

static int get_plane_for_modeset(int32_t drm_fd, int32_t crtc_id, uint32_t *primary_plane_id)
{
	int32_t ret;
	drmModePlaneResPtr plane_res;
	uint32_t plane_id;
	bool found = false;

	/* Need to set this so that we can get Atomic properties */
	ret = drmSetClientCap(drm_fd, DRM_CLIENT_CAP_ATOMIC, 1);
	if (ret) {
		fprintf(stderr, "failed to set client cap: %s\n",
			    strerror(errno));
		return -1;
	}

	plane_res = drmModeGetPlaneResources(drm_fd);
	if (!plane_res) {
		fprintf(stderr, "failed to set plane res: %s\n",
			    strerror(errno));
		return -1;
	}

	/* Will fetch ONLY Primary plane for demo purpose */
	for (int i = 0; i < plane_res->count_planes; i++) {
		drmModePlanePtr p;
		p = drmModeGetPlane(drm_fd, plane_res->planes[i]);

		if (p->crtc_id == crtc_id) {
			drmModeObjectPropertiesPtr props;

			plane_id = p->plane_id;
			props = drmModeObjectGetProperties(drm_fd, p->plane_id,
							DRM_MODE_OBJECT_PLANE);

			for (int j = 0; j < props->count_props; j++) {
				drmModePropertyPtr prop;
				prop = drmModeGetProperty(drm_fd, props->props[j]);
				if (!strcmp(prop->name, "type") &&
				    props->prop_values[j] == DRM_PLANE_TYPE_PRIMARY) {
					*primary_plane_id = p->plane_id;
					found = true;
				}
				drmModeFreeProperty(prop);
			}

			drmModeFreeObjectProperties(props);
		}

		drmModeFreePlane(p);
		if(found)
			break;
	}

	return 0;
}

static void fill_color(void *addr, uint32_t x, uint32_t y, uint32_t width,
		      uint32_t height, uint32_t pitch, uint32_t color)
{
	uint32_t pitch_tmp = pitch / 4;
	uint32_t color_info[width];
	int32_t i;
	uint64_t update_bytes_count;

	update_bytes_count = width * sizeof(uint32_t);

	for (i = 0; i < width; i++)
		color_info[i] = color;

	for (i = y; i < y + height; i++) {
		uint32_t line_pos = i * pitch_tmp;
		uint64_t offset = (line_pos + x) * sizeof(uint32_t);

		memcpy(addr + offset, color_info, update_bytes_count);
	}
}

static uint32_t get_property_id(int fd, drmModeObjectProperties *props,
				const char *name)
{
	drmModePropertyPtr property;
	uint32_t i, id = 0;

	for (i = 0; i < props->count_props; i++) {
		property = drmModeGetProperty(fd, props->props[i]);
		if (!strcmp(property->name, name))
			id = property->prop_id;
		drmModeFreeProperty(property);

		if (id)
			break;
	}

	return id;
}


int do_atomic_commit (int32_t fd, uint32_t crtc_id, uint32_t connector_id, uint32_t plane_id,
		      uint32_t fb_id, drmModeModeInfo mode_info)
{
	int ret = -1;
	drmModeObjectPropertiesPtr props;

	/* Connector prop */
	uint32_t prop_conn_crtc_id;

	/* CRTC prop */
	uint32_t mode_blob_id;
	uint32_t prop_crtc_mode_id;
	uint32_t prop_crtc_active_id;

	/* Plane Properties */
	uint32_t prop_plane_crtc_id;
	uint32_t prop_plane_fb_id;
	uint32_t prop_plane_crtc_x;
	uint32_t prop_plane_crtc_y;
	uint32_t prop_plane_crtc_w;
	uint32_t prop_plane_crtc_h;
	uint32_t prop_plane_src_x;
	uint32_t prop_plane_src_y;
	uint32_t prop_plane_src_w;
	uint32_t prop_plane_src_h;

	drmModeAtomicReqPtr req_ptr = drmModeAtomicAlloc();

	/* Get Connector properties */
	props = drmModeObjectGetProperties(fd, connector_id, DRM_MODE_OBJECT_CONNECTOR);
	prop_conn_crtc_id = get_property_id(fd, props, "CRTC_ID");
	drmModeFreeObjectProperties(props);

	/* Get CRTC  properties */
	props = drmModeObjectGetProperties(fd, crtc_id, DRM_MODE_OBJECT_CRTC);
	prop_crtc_active_id = get_property_id(fd, props, "ACTIVE");
	prop_crtc_mode_id = get_property_id(fd, props, "MODE_ID");
	drmModeFreeObjectProperties(props);

	/* Get plane properties */
	props = drmModeObjectGetProperties(fd, plane_id, DRM_MODE_OBJECT_PLANE);
	prop_plane_crtc_id = get_property_id(fd, props, "CRTC_ID");
	prop_plane_fb_id = get_property_id(fd, props, "FB_ID");
	prop_plane_crtc_x = get_property_id(fd, props, "CRTC_X");
	prop_plane_crtc_y = get_property_id(fd, props, "CRTC_Y");
	prop_plane_crtc_w = get_property_id(fd, props, "CRTC_W");
	prop_plane_crtc_h = get_property_id(fd, props, "CRTC_H");
	prop_plane_src_x = get_property_id(fd, props, "SRC_X");
	prop_plane_src_y = get_property_id(fd, props, "SRC_Y");
	prop_plane_src_w = get_property_id(fd, props, "SRC_W");
	prop_plane_src_h = get_property_id(fd, props, "SRC_H");

	/* Create blob for Mode */
	drmModeCreatePropertyBlob(fd, &mode_info,
				sizeof(mode_info), &mode_blob_id);

	/* Attach the display pipeline properties to Atomic Commit request */
	drmModeAtomicAddProperty(req_ptr, crtc_id, prop_crtc_active_id, 1);
	drmModeAtomicAddProperty(req_ptr, crtc_id, prop_crtc_mode_id, mode_blob_id);
	drmModeAtomicAddProperty(req_ptr, connector_id, prop_conn_crtc_id, crtc_id);

	/* Attach Plane properties */
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_crtc_id, crtc_id);
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_fb_id, fb_id);
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_crtc_x, 0);
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_crtc_y, 0);
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_crtc_w, mode_info.hdisplay);
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_crtc_h, mode_info.vdisplay);
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_src_x, 0 << 16);
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_src_y, 0 << 16);
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_src_w, mode_info.hdisplay << 16);
	drmModeAtomicAddProperty(req_ptr, plane_id, prop_plane_src_h, mode_info.vdisplay << 16);

	ret = drmModeAtomicCommit(fd, req_ptr, DRM_MODE_ATOMIC_ALLOW_MODESET, NULL);
	if (ret < 0) {
		printf("atomic commit failed, %d\n", errno);
		return -1;
	}

	drmModeAtomicFree(req_ptr);

	return 0;
}

int main(int argc, char **argv)
{
	int drm_fd;
	int32_t ret = -1;
	drmModeResPtr res_ptr = NULL;
	drmModeConnectorPtr conn_ptr = NULL;
	uint32_t width, height;
	uint32_t bo_size, bo_pitch, bo_handle, bo_fbid;
	uint32_t crtc_id;
	uint32_t plane_id;
	void *addr;

	/* Open the drm device (Display/GPU/GEM) */
	drm_fd = open("/dev/dri/card0", O_RDWR | O_CLOEXEC);
	if (drm_fd < 0) {
		fprintf(stderr, "Error: failed to open DRM device:%s\n", strerror(errno));
		return -1;
	}

	/* Get the resources (CRTC, ENCODER, CONNECTOR etc) related to display controller */
	res_ptr = drmModeGetResources(drm_fd);
	if (res_ptr == NULL) {
		printf("Error: Getting DRM resources\n");
		return -1;
	}

	/* Find the connector on which display panel is connected */
	conn_ptr = get_connector_for_modeset(drm_fd, res_ptr);
	if (conn_ptr == NULL) {
		printf("Error: Getting connector for modeset resources\n");
		return -1;
	}

	/* Need CRTC on which selected mode (resolution) will be set */
	ret = get_crtc_for_modeset(drm_fd, res_ptr, conn_ptr, &crtc_id);
	if(ret == -1) {
		printf("Error: Getting CRTC Id\n");
		return -1;
	}

	/* Just selecting the first available mode for demo purpose */
	height = conn_ptr->modes[0].vdisplay;
	width = conn_ptr->modes[0].hdisplay;

	printf("Selected Width: %u, Height:%u\n", width, height);


	/* Create a buffer and make it as framebuffer, to this buffer we will fill some colors */
	ret = create_display_fb(drm_fd, width, height, &bo_fbid, &addr);
	if(ret ==-1){
		printf("Error: Creating display_fb\n");
		return -1;
	}

	/* Fill some colors to the buffer created above */
	fill_color(addr, width / 2, height / 2,
		  width /2, height/2, width, rand());

	/* find the primary plane, we will use primary plane for this test and attach the create display FB to this primary plane */
	ret = get_plane_for_modeset(drm_fd, crtc_id, &plane_id);
	if(ret ==-1){
		printf("Error: Get plane for modeset\n");
		return -1;
	}

	/* Framebuffer is ready, Commit it to display HW, after the next VBLANK it will be shown */
	ret = do_atomic_commit (drm_fd, crtc_id, conn_ptr->connector_id, plane_id, bo_fbid, conn_ptr->modes[0]);
	if(ret ==-1){
		printf("Error: Atomic commit\n");
		return -1;
	}

	/* Delay so that rectangle can be visible on screen for few seconds */
	sleep (5);

	return 0;
}

